import React from 'react';


class SearchBar extends React.Component {

    state = {
        term : ""
    }

    onInputChange = (event) => {
        this.setState({
            term: event.target.value
        })

        this.props.onSearch(event.target.value)
    }

    render() {
        return (
            <div className="search-bar">
                <input onChange={this.onInputChange} value={this.state.term} placeholder="Search.."/>
            </div>
        )
    }
}


export default SearchBar;